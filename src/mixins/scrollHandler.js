export default {
	methods: {
		onScrollEnd() {
			console.warn('you must override');
		},
		onScroll(event) {
			const container = event.target;
			if ( container.clientHeight + container.scrollTop >= container.scrollHeight )
				this.onScrollEnd();
		}
	},
}