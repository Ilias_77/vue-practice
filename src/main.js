import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router'
import router from './router/index'
import store from './store/index'

import VModal from 'vue-js-modal'
import ApiPlugin from './plugins/api'
import LoadPlugin from './plugins/load'

Vue.use(VueRouter)
Vue.use(VModal)
Vue.use(ApiPlugin)
Vue.use(LoadPlugin)

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)


import SimplePlugin from './plugins/simplePlugin'
Vue.use(SimplePlugin)

// import marked from 'marked'
// Vue.filter('marked', marked)

import '@/assets/styles/main.scss'

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')