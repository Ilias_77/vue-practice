export default function(instance) {
	return {
		signIn(payload) {
			// return instance.post('auth/sign_in', payload)
			return JSON.stringify(payload);
		},
		signUp(payload) {
			// return instance.post('auth/sign_up', payload)
			return JSON.stringify(payload);
		},
		logout() {
			// return instance.delete('auth/logout')
			return instance.delete('auth/logout')
		}
	}
}