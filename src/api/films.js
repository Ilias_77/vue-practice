import films from '../mocks/films'

export default function() {
// export default function(instance) {
	return {
		get(id) {
			// return instance.get(`films/${id}`);
			return films.find(film => film.id == id);
		},
		getAll() {
			// return instance.get(`films`);
			return films;
		}
	}
}